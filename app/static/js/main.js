$( document ).ready(function() {

    const  api_base_url = 'http://127.0.0.1:8000/';
    let csrf_token = document.getElementsByName('csrfmiddlewaretoken')[0].value;

    // <!----------------конструктор таблицы заказа НАЧАЛО---------------------------------------->

    //let korobka_th = document.querySelector('#hidden-table .th-korobka').outerHTML;
    //let korobka_td = document.querySelector('#hidden-table .td-korobka').outerHTML;

    let petli_th = document.querySelector('#hidden-table .th-petli').outerHTML;
    let petli_td = document.querySelector('#hidden-table .td-petli').outerHTML;

    let zamok_th = document.querySelector('#hidden-table .th-zamok').outerHTML;
    let zamok_td = document.querySelector('#hidden-table .td-zamok').outerHTML;

    let zazor_snizy_th = document.querySelector('#hidden-table .th-zazor-snizy').outerHTML;
    let zazor_snizy_td = document.querySelector('#hidden-table .td-zazor-snizy').outerHTML;

    let torec_th = document.querySelector('#hidden-table .th-torec').outerHTML;
    let torec_td = document.querySelector('#hidden-table .td-torec').outerHTML;
/*
    $( "#dvernaya_korobka" ).click(function() {

        if($("#dvernaya_korobka").prop("checked") == true) {
            if (document.querySelector('#specification-table-wrapper .th-korobka') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(korobka_th);
                $("#specification-table-wrapper tr:not(:first)").append(korobka_td);
            }
        }else {
            if (document.querySelector('#specification-table-wrapper .th-korobka') != null && $("#frezerovka_korobki_por_petli_i_ovesky_zamka").prop("checked") == false){
                $('#specification-table-wrapper .td-korobka').remove();
                $('#specification-table-wrapper .th-korobka').remove();
            }
        }
    });
*/
    $( "#torec_karkas_polotna" ).click(function() {

        if($("#torec_karkas_polotna").prop("checked") == true) {
            if (document.querySelector('#specification-table-wrapper .th-torec') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(torec_th);
                $("#specification-table-wrapper tr:not(:first)").append(torec_td);
            }
        }else {
            if (document.querySelector('#specification-table-wrapper .th-torec') != null){
                $('#specification-table-wrapper .td-torec').remove();
                $('#specification-table-wrapper .th-torec').remove();
            }
        }
    });

    $( "#petli" ).click(function() {

        if($("#petli").prop("checked") == true) {
            $('#specification-table-wrapper .td-petli').remove();
            $('#specification-table-wrapper .th-petli').remove();
            $("#specification-table-wrapper tr:lt(1)").append(petli_th);
            $("#specification-table-wrapper tr:not(:first)").append(petli_td);
        }else {
            if (document.querySelector('#specification-table-wrapper .th-petli') != null) {
                $('#specification-table-wrapper .td-petli').remove();
                $('#specification-table-wrapper .th-petli').remove();
            }
        }
    });

    $( "#zamok_and_otvestka" ).click(function() {

        if($("#zamok_and_otvestka").prop("checked") == true) {
            $('#specification-table-wrapper .td-zamok').remove();
            $('#specification-table-wrapper .th-zamok').remove();
            $("#specification-table-wrapper tr:lt(1)").append(zamok_th);
            $("#specification-table-wrapper tr:not(:first)").append(zamok_td);
        }else {
            if (document.querySelector('#specification-table-wrapper .th-zamok') != null) {
                $('#specification-table-wrapper .td-zamok').remove();
                $('#specification-table-wrapper .th-zamok').remove();
            }
        }
    });

    $( "#frezerovka_korobki_por_petli_i_ovesky_zamka" ).click(function() {

        if($("#frezerovka_korobki_por_petli_i_ovesky_zamka").prop("checked") == true) {
            if (document.querySelector('#specification-table-wrapper .th-zamok') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(zamok_th);
                $("#specification-table-wrapper tr:not(:first)").append(zamok_td);
            }
            if (document.querySelector('#specification-table-wrapper .th-petli') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(petli_th);
                $("#specification-table-wrapper tr:not(:first)").append(petli_td);
            }
            if (document.querySelector('#specification-table-wrapper .th-korobka') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(korobka_th);
                $("#specification-table-wrapper tr:not(:first)").append(korobka_td);
            }
        }else {
            if (document.querySelector('#specification-table-wrapper .th-zamok') != null && $("#zamok_and_otvestka").prop("checked") == false && $("#frezerovka_torca_or_karkasa_pod_petli_i_zamok").prop("checked") == false) {
                $('#specification-table-wrapper .td-zamok').remove();
                $('#specification-table-wrapper .th-zamok').remove();
            }
            if (document.querySelector('#specification-table-wrapper .th-petli') != null && $("#petli").prop("checked") == false && $("#frezerovka_torca_or_karkasa_pod_petli_i_zamok").prop("checked") == false) {
                $('#specification-table-wrapper .td-petli').remove();
                $('#specification-table-wrapper .th-petli').remove();
            }
            if (document.querySelector('#specification-table-wrapper .th-korobka') != null && $("#dvernaya_korobka").prop("checked") == false && $("#frezerovka_torca_or_karkasa_pod_petli_i_zamok").prop("checked") == false) {
                $('#specification-table-wrapper .td-korobka').remove();
                $('#specification-table-wrapper .th-korobka').remove();
            }
        }
    });

    $( "#frezerovka_torca_or_karkasa_pod_petli_i_zamok" ).click(function() {

        if($("#frezerovka_torca_or_karkasa_pod_petli_i_zamok").prop("checked") == true) {
            if (document.querySelector('#specification-table-wrapper .th-zamok') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(zamok_th);
                $("#specification-table-wrapper tr:not(:first)").append(zamok_td);
            }
            if (document.querySelector('#specification-table-wrapper .th-petli') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(petli_th);
                $("#specification-table-wrapper tr:not(:first)").append(petli_td);
            }
            if (document.querySelector('#specification-table-wrapper .th-korobka') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(korobka_th);
                $("#specification-table-wrapper tr:not(:first)").append(korobka_td);
            }
        }else {
            if (document.querySelector('#specification-table-wrapper .th-zamok') != null && $("#zamok_and_otvestka").prop("checked") == false && $("#frezerovka_korobki_por_petli_i_ovesky_zamka").prop("checked") == false) {
                $('#specification-table-wrapper .td-zamok').remove();
                $('#specification-table-wrapper .th-zamok').remove();
            }
            if (document.querySelector('#specification-table-wrapper .th-petli') != null && $("#petli").prop("checked") == false && $("#frezerovka_korobki_por_petli_i_ovesky_zamka").prop("checked") == false) {
                $('#specification-table-wrapper .td-petli').remove();
                $('#specification-table-wrapper .th-petli').remove();
            }
            if (document.querySelector('#specification-table-wrapper .th-korobka') != null && $("#dvernaya_korobka").prop("checked") == false && $("#frezerovka_korobki_por_petli_i_ovesky_zamka").prop("checked") == false) {
                $('#specification-table-wrapper .td-korobka').remove();
                $('#specification-table-wrapper .th-korobka').remove();
            }
        }
    });

    $("#pilim_korobky_v_razmer_polotna").click(function() {

        if($("#pilim_korobky_v_razmer_polotna").prop("checked") == true) {
            if (document.querySelector('#specification-table-wrapper .th-zazor-snizy') == null) {
                $("#specification-table-wrapper tr:lt(1)").append(zazor_snizy_th);
                $("#specification-table-wrapper tr:not(:first)").append(zazor_snizy_td);
            }

        }else {
            if (document.querySelector('#specification-table-wrapper .th-zazor-snizy') != null) {
                $('#specification-table-wrapper .td-zazor-snizy').remove();
                $('#specification-table-wrapper .th-zazor-snizy').remove();
            }
        }
    });

    //Удаляем ряд из заказа при клике на крестик
    console.log('пук1');
    $('.delete-icon-wrapper img').click(function(){
        console.log('пук2');
        $(this).closest('tr').remove();
    });


    //Добавляем новые строки в таблицу заказа
    $("#add_new_string").click(function () {
        let row_tmpl = document.querySelector(".order-row").outerHTML;
        $("#specification-table-wrapper tbody").append(row_tmpl);
        let rows_count = $("#specification-table-wrapper table tbody").children('tr').length;
        rows_count -= 1;
        $("#specification-table-wrapper tbody .td-string-number:last").text(rows_count);
    });
// <-------------------------конструктор таблицы заказа КОНЕЦ   ---------------------------->

    //сбор и отправка данных на сервер из таблицы заказа
    $("#order-details-wrapper").submit(function () {

          let order_base_info = {};
          let order_items = {};

          $('#order_description_wrapper').find(':input').each(function(i, item) {
            order_base_info[$(item).attr('id')] = $(item).val();
          });

          $('.js-col-complictation').find(':input').each(function(i, item) {
            order_base_info[$(item).attr('id')] = $(item).prop('checked');
          });

          $('.obrabotka-wrap').find(':input').each(function(i, item) {
              if ($(item).attr('type') == 'number' ) {
                  order_base_info[$(item).attr('id')] = $(item).val();
              }else {
                  order_base_info[$(item).attr('id')] = $(item).prop('checked');
              }
          });

          $('#specification-table-wrapper tr:not(:first)').each(function (i) {
              let order_item = {};
              $(this).each(function () {
                    order_item["position_in_order"] = $(this).find(".td-string-number").text();
                    order_item["sticker"] = $(this).find('.descr-input').val();
                    order_item["polotno_height"] = $(this).find('.polonto-dlina').val();
                    order_item["polotno_width"] = $(this).find('.polonto-shirina').val();
                    order_item["storona_otkr"] = $(this).find('.select-storona-otkr').val();
                    order_item["color"] = $(this).find('.select-color').val();
                    order_item["zamok"] = $(this).find('.select-zamki').val();
                    order_item["petli"] = $(this).find('.select-petli').val();
                    order_item["petli_count"] = $(this).find('.petli-count').val();
                    order_item["korobka"] = $(this).find('.select-korobki').val();
                    order_item["top_poperechina"] = $(this).find('.top-poperechina').prop('checked');
              });
              order_items[i] = order_item;
          });

          console.log(order_base_info);


        $.ajax({
            type: 'POST',
            url: api_base_url+'api/order/',
            data: order_base_info,
            dataType: 'json',
            headers:{"X-CSRFToken": csrf_token},
            success: function(response){ // если запрос успешен
                $("#order_id").val(response.id);
                $("#order_id_wrapper").show();
                $.each(order_items, function( key, value ) {
                    value['order_id'] = $("#order_id").val();
                    $.ajax({
                        type: 'POST',
                        url: api_base_url+'api/orderitem/',
                        data: value,
                        dataType: 'json',
                        headers:{"X-CSRFToken": csrf_token},
                        success: function(response){ // если запрос успешен вызываем функцию

                        },
                        error: function() {
                            alert("Ошибка");
                        }
                    });
                });

                let data = {};
                data['order_id'] = $("#order_id").val();
                let type = false;
                data['type'] = type;
                console.log("data, запрос на updateOrderItems (коробки): ", data);
                $.ajax({
                    type: 'POST',
                    url: api_base_url+'updateOrderItems/',
                    data: data,
                    dataType: 'json',
                    headers:{"X-CSRFToken": csrf_token},
                    beforeSend: function(){
                        $('#order_loader_wrapper').html('<img id="order_loader" src="/static/img/loader.gif">');
                    },
                    success: function(response){ // если запрос успешен вызываем функцию
                        $("#order_loader").remove();
                        let parsed_result = JSON.stringify(response);
                        let raskroy_html = raskroy_render(response);
                        $("#js-raskroy").empty();
                        $("#js-raskroy").append(parsed_result);
                        $("#js-raskroy").append(raskroy_html);
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $("#js-raskroy").offset().top
                        }, 2000);
                    },
                    error: function() {
                        $("#order_loader").remove();
                        alert("Ошибка updateOrderItems при получении коробок");
                    }
                });

                let data2 = {}
                let type2 = $("#torec_karkas_polotna").prop('checked');
                data2['order_id'] = $("#order_id").val();
                console.log("type: ", type2);
                data2['type'] = type2;
                if (type2 == true){
                    $.ajax({
                        type: 'POST',
                        url: api_base_url+'updateOrderItems/',
                        data: data2,
                        dataType: 'json',
                        headers:{"X-CSRFToken": csrf_token},
                        beforeSend: function(){
                            $('#order_loader_wrapper').html('<img id="order_loader" src="/static/img/loader.gif">');
                        },
                        success: function(response){ // если запрос успешен вызываем функцию
                            $("#order_loader").remove();
                            let parsed_result = JSON.stringify(response);
                            let raskroy_html = raskroy_render(response);
                            //$("#js-raskroy").empty();
                            $("#js-raskroy").append(parsed_result);
                            $("#js-raskroy").append(raskroy_html);
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $("#js-raskroy").offset().top
                            }, 2000);
                        },
                        error: function() {
                            $("#order_loader").remove();
                            alert("Ошибка updateOrderItems при получении торцов");
                        }
                    });
                }

            },
            error: function() {
                alert("Ошибка. Возможно вы не залогинились?");
            }
        });

    });

    $("#sendtestapi").click(function () {
        let dict = {};
        let name1 = String($("#name1").val());
        let name2 = String($("#name2").val());
        dict['name1'] = name1;
        dict['name2'] = name2;
        $.ajax({
            type: 'POST',
            url: api_base_url+'app/api/testapiregister/',
            data: dict,
            dataType: 'json',
            headers:{"X-CSRFToken": csrf_token},
            success: function(response, xhr){ // если запрос успешен вызываем функцию
                alert("Успешно добавлен");
            },
            error: function() {
                alert("Ошибка");
            }
        });

    });

    $('input:checked').prop('checked', false); //обнуляем чекбоксы при перезагрузке страницы
    //но оставляем некоторые отмеченными
    $('#dvernaya_korobka').prop('checked', true);
    //$('#dlya_dverey_bez_verhn_poperechini_plus').prop('checked', true);
    $('.top-poperechina').prop('checked', true);

});


function raskroy_render(raskroy_json) {

    let raskroy_full_html = "";
    let raskroy_one_list_end = "</div></div>";
    let raskroy_one_list_start;
    let i;

    console.log("Перебираем raskroy_json...");
    for (let key in raskroy_json) {

      console.log("item", key, raskroy_json[key]);
      for (i=0; i < raskroy_json[key]['raskroy'].length; i++) {

          console.log('raskroy_json[key][\'raskroy\'][i][\'details\']: ', raskroy_json[key]['raskroy'][i]['details']);
          if (raskroy_json[key]['raskroy'][i]['details'].length == 0 ) {
              console.log("пустой массив details ", raskroy_json[key]['raskroy'][i]['details']);
              break;
          }

          let size_details_on_list = 0;
          //var korobka_name = raskroy_json[key]['korobka_name'];
          raskroy_one_list_start = "" +
              "                        <div class=\"one-list\">\n" +
              `                            <span class=\"profil_name\">${raskroy_json[key]['korobka_name']}</span>` +
              "                            <div class=\"raskroy-wrapper\">";

          for (let detail in raskroy_json[key]['raskroy'][i]['details']) {
              console.log("i: ", i);
              if (raskroy_json[key]['raskroy'][i]['details'][detail]['zapils_count'] == 0) {
                  $("#raskroy_tpls .raskroy_item_tpl_zap_0_wrapper .pos-numb-val").text(raskroy_json[key]['raskroy'][i]['details'][detail]['position_in_order']);
                  $("#raskroy_tpls .raskroy_item_tpl_zap_0_wrapper .size-val").text(raskroy_json[key]['raskroy'][i]['details'][detail]['size']);
                  console.log('zapils_count = 0');
                  raskroy_one_list_start += document.querySelector('#raskroy_tpls .raskroy_item_tpl_zap_0_wrapper').innerHTML;
                  size_details_on_list += raskroy_json[key]['raskroy'][i]['details'][detail]['size'];
              }
              if (raskroy_json[key]['raskroy'][i]['details'][detail]['zapils_count'] == 1) {
                  $("#raskroy_tpls .raskroy_item_tpl_zap_1_wrapper .pos-numb-val").text(raskroy_json[key]['raskroy'][i]['details'][detail]['position_in_order']);
                  $("#raskroy_tpls .raskroy_item_tpl_zap_1_wrapper .size-val").text(raskroy_json[key]['raskroy'][i]['details'][detail]['size']);
                  console.log('zapils_count = 1');
                  raskroy_one_list_start += document.querySelector('#raskroy_tpls .raskroy_item_tpl_zap_1_wrapper').innerHTML;
                  size_details_on_list += raskroy_json[key]['raskroy'][i]['details'][detail]['size'];
              }
              if (raskroy_json[key]['raskroy'][i]['details'][detail]['zapils_count'] == 2) {
                  $("#raskroy_tpls .raskroy_item_tpl_zap_2_wrapper .pos-numb-val").text(raskroy_json[key]['raskroy'][i]['details'][detail]['position_in_order']);
                  $("#raskroy_tpls .raskroy_item_tpl_zap_2_wrapper .size-val").text(raskroy_json[key]['raskroy'][i]['details'][detail]['size']);
                  console.log('zapils_count = 2');
                  raskroy_one_list_start += document.querySelector('#raskroy_tpls .raskroy_item_tpl_zap_2_wrapper').innerHTML;
                  size_details_on_list += raskroy_json[key]['raskroy'][i]['details'][detail]['size'];
              }
              console.log("profil_number: ", raskroy_json[key]['raskroy'][i]['profil_number']);
              console.log("деталь: ", raskroy_json[key]['raskroy'][i]['details'][detail]);
          }
          let ostatok_mm = raskroy_json[key]['polotno_size'] - size_details_on_list;
           $("#raskroy_tpls .raskroy_item_tpl_ostatok_wrapper .ostatok-val").text(ostatok_mm);
           raskroy_one_list_start += document.querySelector('#raskroy_tpls .raskroy_item_tpl_ostatok_wrapper').innerHTML;
          raskroy_one_list_start += raskroy_one_list_end;
          raskroy_full_html += raskroy_one_list_start;
          //console.log(raskroy_full_html);
      }
    }
    return(raskroy_full_html);
}
from django.contrib import admin
from . import models

admin.site.register(models.Torec)
admin.site.register(models.TorecOptions)
admin.site.register(models.Pokritie)
admin.site.register(models.Korobka)
admin.site.register(models.KorobkaOptions)
admin.site.register(models.KorobkaPolkiDlyaPetel)
admin.site.register(models.KorobkaOpenByTorec)
admin.site.register(models.KorobkaReversByTorec)
admin.site.register(models.ProfilKarkasaPolotna)
admin.site.register(models.ProfilKarkasaPolotnaOptions)
admin.site.register(models.SkritiePetli)
admin.site.register(models.Zamok)
admin.site.register(models.Yplotnitel)
admin.site.register(models.YplotnitelCombinations)
admin.site.register(models.Ygolki)
admin.site.register(models.MontajniePlastini)
admin.site.register(models.ShtukaturnayaSetka)
admin.site.register(models.Obrabotka)
admin.site.register(models.ObrabotkaTest)
admin.site.register(models.TestModelForApi)
#admin.site.register(models.Order)

@admin.register(models.Order)
class Order(admin.ModelAdmin):
    list_display = (
        'id',
        'manager',
        'otvetstvenniy_za_proizvodstvo',
        'description',
        'client',
        'created_at',
        'updated_at',

        # комплектация
        'dvernaya_korobka',
        'torec_karkas_polotna',
        'petli',
        'zamok_and_otvestka',
        'yplotnitel',
        'syhori_ygolki',
        'montajnie_plastini',
        'shtykatyrnaya_setka',

        # обработка
        'bez_obrabotki',
        'pilim_dlya_transportnoi',
        'mehobrabotka',
        'pilim_korobky_v_razmer_polotna',
        'pilim_torec_v_razmer_polotna_s_zapasom',
        'pilim_torec_v_razmer_polotna',
        'zapas_mm',
        'pilim_torec_v_razmer_polotna_s_zapilom',
        'otrezaem_kontatknie_pyatna',
        'kontaktnie_pyatna_mm',
        'frezerovka_korobki_por_petli_i_ovesky_zamka',
        'frezerovka_torca_or_karkasa_pod_petli_i_zamok',
        'frezerovka_pod_nestandartnyy_fyrnityry',
        'na_niz_revers_poloten_pryamoi_torec',
        'na_verh_poloten_bez_poperechini_pryamoi_torec',
        'dlya_dverey_bez_verhn_poperechini_plus',
        'dlya_dverey_bez_verhnei_poperechini_plus_mm',
        #'raskroy_json'

    )


@admin.register(models.OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'order_id',
        'position_in_order',
        'sticker',
        'polotno_height',
        'polotno_width',
        'storona_otkr',
        'color',
        'zamok',
        'petli',
        'petli_count',
        'korobka',
        'created_at',
        'updated_at',
        'vertikal_length1',
        'vertikal_length2',
        'horizontal_length',
        'top_poperechina',
        'shirina_v_proeme'
    )


@admin.register(models.OrderDetails)
class OrderDetailsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'order',
        'order_item',
        'size',
        'zapils_count',
        'type',
        'profil_torca',
        'used'
    )


admin.site.register(models.MehObrabotkaPrice)
admin.site.register(models.MyConstants)


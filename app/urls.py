from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'testapiregister', views.TestApiViewSet)
router.register(r'order', views.OrderViewSet)
router.register(r'orderitem', views.OrderItemViewSet)

urlpatterns = [
    path(
        '',
        views.index,
        name='index'),
    path(
        'api/',
        include(
            router.urls)),
    path(
        'api/apitest/',
        views.TestApiViewSet,
        name='apitestname'),
    path(
        'api/order/',
        views.OrderViewSet,
        name='order'),
    path(
        'api/orderitem/',
        views.OrderItemViewSet,
        name='orderitem'),
    path(
        'api/api-auth/',
        include(
            'rest_framework.urls',
            namespace='rest_framework')),
    path(
        'updateOrderItems/',
        views.updateOrderItems,
        name='updateOrderItems')]

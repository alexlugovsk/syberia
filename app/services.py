import copy
from .models import Order, OrderItem, MyConstants, OrderDetails, Torec, Korobka, OrderDetails
from . import helpers
from .busines_services.lineyniy_raskroy import optimizacia6
from django.http import JsonResponse
import time


def orderItemSaveDetails(orderitem, type=False, constants=MyConstants.objects.get(pk=1)):
    # получим детали заказа, которому пренадлежит позиция
    order = Order.objects.get(pk=orderitem.order_id)
    print(f"services.orderItemSaveDetails order: {order}")
    print(f"services.orderItemSaveDetails order.torec_karkas_polotna = {order.torec_karkas_polotna}")
    horizontal_length = None
    vertikal_length = None
    torec_top = None
    torec_bot = None
    torec_left = None
    torec_right = None
    profil_torca = TorecAutoChoice(
        storona_otkr=orderitem.storona_otkr,
        korobka_id=orderitem.korobka.id)
    if(orderitem.top_poperechina):
        vertikal_length = orderitem.polotno_height + constants.zazor_sverhy_est_poperchnaya + \
            constants.zazor_snizy  # длина каждой из двух вертикальных деталей(стоек коробки)
        horizontal_length = orderitem.polotno_width + \
            constants.zazor_sleva + constants.zazor_sprava  # длина поперечины
    else:
        if (order.dlya_dverey_bez_verhn_poperechini_plus):
            vertikal_length = orderitem.polotno_height + constants.zazor_sverhy_net_poperechnoi + \
                constants.zazor_snizy + order.dlya_dverey_bez_verhnei_poperechini_plus_mm
        else:
            vertikal_length = orderitem.polotno_height + \
                constants.zazor_sverhy_net_poperechnoi + constants.zazor_snizy
    # if (order.pilim_korobky_v_razmer_polotna_s_zapilom == True):

    if (order.torec_karkas_polotna == True):
        print("services.orderItemSaveDetails order.torec_karkas_polotna == True вроде")
        if (order.pilim_torec_v_razmer_polotna == True):
            print("services.orderItemSaveDetails order.pilim_torec_v_razmer_polotna == True вроде")
            if (order.otrezaem_kontatknie_pyatna):
                torec_top = orderitem.polotno_width + order.zapas_mm + order.kontaktnie_pyatna_mm
                torec_bot = orderitem.polotno_width + order.zapas_mm + order.kontaktnie_pyatna_mm
                torec_left = orderitem.polotno_height + \
                    order.zapas_mm + order.kontaktnie_pyatna_mm
                torec_right = orderitem.polotno_height + \
                    order.zapas_mm + order.kontaktnie_pyatna_mm
            else:
                torec_top = orderitem.polotno_width + order.zapas_mm
                torec_bot = orderitem.polotno_width + order.zapas_mm
                torec_left = orderitem.polotno_height + order.zapas_mm
                torec_right = orderitem.polotno_height + order.zapas_mm

            createDetail(orderitem.order_id, orderitem, torec_top, type=True)
            # createDetail(orderitem.order_id, orderitem, torec_bot, type=True)
            createDetail(orderitem.order_id, orderitem, torec_left, type=True)
            createDetail(orderitem.order_id, orderitem, torec_right, type=True)

        if (order.pilim_torec_v_razmer_polotna_s_zapilom):
            if (order.otrezaem_kontatknie_pyatna):
                torec_top = orderitem.polotno_width + order.kontaktnie_pyatna_mm
                torec_bot = orderitem.polotno_width + order.kontaktnie_pyatna_mm
                torec_left = orderitem.polotno_height + order.kontaktnie_pyatna_mm
                torec_right = orderitem.polotno_height + order.kontaktnie_pyatna_mm
            else:
                torec_top = orderitem.polotno_width
                torec_bot = orderitem.polotno_width
                torec_left = orderitem.polotno_height
                torec_right = orderitem.polotno_height

            if (orderitem.top_poperechina == True):
                createDetail(
                    orderitem.order_id,
                    orderitem,
                    torec_top,
                    type=True,
                    zapils_count=2)
            # createDetail(
            #     orderitem.order_id,
            #     orderitem,
            #     torec_bot,
            #     type=True,
            #     zapils_count=2)
            createDetail(
                orderitem.order_id,
                orderitem,
                torec_left,
                type=True,
                zapils_count=2)
            createDetail(
                orderitem.order_id,
                orderitem,
                torec_right,
                type=True,
                zapils_count=2)

    sizes = {}
    if (vertikal_length is not None):
        orderitem.vertikal_length1 = vertikal_length
        orderitem.vertikal_length2 = vertikal_length
        sizes['vertikal_length1'] = vertikal_length
        sizes['vertikal_length2'] = vertikal_length
        if (orderitem.top_poperechina == False):
            createDetail(orderitem.order_id, orderitem, vertikal_length)
            createDetail(orderitem.order_id, orderitem, vertikal_length)
        else:
            createDetail(
                orderitem.order_id,
                orderitem,
                vertikal_length,
                zapils_count=1)
            createDetail(
                orderitem.order_id,
                orderitem,
                vertikal_length,
                zapils_count=1)

    if (horizontal_length is not None):
        orderitem.horizontal_length = horizontal_length
        sizes['horizontal_length'] = horizontal_length
        createDetail(
            orderitem.order_id,
            orderitem,
            horizontal_length,
            zapils_count=2)
    orderitem.save()

    sizes = list(sizes.values())
    # sizes.append(orderitem.korobka.id)
    print(f"orderItemSaveDetails.sizes: {sizes}")
    return sizes

# type = False - деталь коробки, type = True - один из торцов
def createDetail(
        order,
        order_item,
        size,
        type=False,
        zapils_count=0,
        used=False,
        profil_torca=None):
    detail = OrderDetails()
    detail.order = order
    detail.order_item = order_item
    detail.size = size
    detail.type = type
    detail.zapils_count = zapils_count
    # соспоставили ли мы уже эту деталь с тем что отрыгнул нам алгоритм раскроя
    detail.used = used
    detail.profil_torca = profil_torca
    detail.save()

def TorecAutoChoice(storona_otkr, korobka_id):
    """
    автоматическое определение необходимого торца
    в зависимости от коробки и типа открывания двери
    Ксм1, Ксм4 прямое открывание - Торец 06 профиль. Реверс Торец 08
    Ксм4 прямое - Торец 06. Реверс Торец 05м
    """
    torec = None
    if (storona_otkr == 1 or storona_otkr == 2):
        torec = Torec.objects.get(name="Торец 06")
    elif(storona_otkr == 3 or storona_otkr == 4) and (korobka_id == 1):
        torec = Torec.objects.get(name="Торец 08")
    elif (storona_otkr == 3 or storona_otkr == 4) and (korobka_id == 2):
        torec = Torec.objects.get(name="Торец 05")
    else:
        print("services.TorecAutoChoice. Ошибка автовыбора торца")
    print(
        f"services.TorecAutoChoice выбранный торец(id): {torec}, storona_otkr: {storona_otkr}, korobka_id: {korobka_id}")
    return torec

def getResult(korobki_in_order, order_id, type, constants=MyConstants.objects.get(pk=1)):
    """
    генерирует словарь с информацией о раскрое
    """
    result_full = {}
    for korobka in korobki_in_order:
        # все размеры деталей, относящиеся к одному профилю(коробке)
        # конкретного заказа
        order_details_size = []
        # получим длину профиля (и остальную инфу о коробке)
        korobka_dlina = Korobka.objects.get(pk=korobka['korobka'])
        print(f"korobka_dlina: {korobka_dlina}")
        items_by_korobka = OrderItem.objects.filter(
            korobka=korobka['korobka'], order_id=order_id).values('id')
        print(f"items_by_korobka: {items_by_korobka}")
        for item_by_korobka in items_by_korobka:
            orderitem = OrderItem.objects.get(pk=item_by_korobka['id'])
            print(f"orderitem: {orderitem}")
            order_details_size += orderItemSaveDetails(orderitem, type)
        print(f"order_details_size: {order_details_size}")
        # подробим кортеж по 14 элементов, чтобы нормально скормить алгоритму
        # раскроя
        split_tuple = helpers.splitTuple(tuple(order_details_size), 14)
        print(f"split_tuple: {split_tuple}")
        raskroy = []
        for i in split_tuple:
            print(f" i in split typle: {i}")
            get_one_raskroy = optimizacia6.optimization_cutting(
                i, korobka_dlina.dlina_mm, constants.tolshina_pila_mm)
            get_one_raskroy.get_raskroy()
            luchiy_raskroy = get_one_raskroy.luchiy_raskroy
            print(f"luchiy_raskroy: {tuple(luchiy_raskroy)}")

            #обработка исключительной ситуации, когда у нас под одну
            #группу раскроя попадает только одна позиция заказа
            if isinstance(luchiy_raskroy[0], int):
                luchiy_raskroy_correct = ([luchiy_raskroy])
                raskroy.append(tuple(luchiy_raskroy_correct))
            else:
                raskroy.append(tuple(luchiy_raskroy))
        raskroy = tuple(raskroy)

        result = {}
        result['korobka_name'] = korobka_dlina.name
        result['polotno_size'] = korobka_dlina.dlina_mm
        result['raskroy'] = raskroy
        print(f"raskroy: {raskroy}")
        print(f"result: {result}")
        result_full[korobka_dlina.id] = result
    return result_full

def findDitailInDb(order_id, data, type=False):
    """
    data - то что отрыгнул нам алгоритм раскроя(все пулы по одному ордеру)
    дополняем данные о раскрое доп. информацией
    """
    new_data = copy.deepcopy(data)
    print(f"new_data copy data: {new_data}")
    for i in new_data.keys():
        if (new_data.get(i) != None):
            print(f"new_data.get(i).get('raskroy') {new_data.get(i).get('raskroy')}")
            new_data.get(i).update({'raskroy':{}})
    print(f"findDitailInDb new_data before clean: {new_data}")

    for i in data.keys():
        data2 = data.get(i)
        if (data2 != None):
            data3 = data2.get('raskroy')
            korobka_name = data2.get('korobka_name')
            details_list = []
            print(f"data3: {data3}")
            for i2 in data3:
                detail_and_profil_sublist_else = []
                print(f"i2: {i2}")
                #i2 = helpers.delete_empty_sublist(i2)
                #print(f"clear i2: {i2}")
                profil_number = 1
                for i3 in i2:
                    detail_and_profil_sublist = []
                    print(f"i3: {i3}")
                    for i4 in i3:
                        print(f"i4: {i4}")
                        print(f"findDitailInDb type: {type}")
                        print(f"findDitailInDb i4: {i4}")
                        print(f"findDitailInDb order_id: {order_id}")
                        print(f"findDitailInDb korobka_name: {korobka_name}")
                        #time.sleep(1)
                        detail = OrderDetails.objects.filter(type=type, size=i4, order=order_id, used=False, order_item__korobka__name=korobka_name).first()
                        print(f"Найденая деталь: {detail}")
                        detail.used = True
                        detail.save()
                        detail_dict = {
                            "detail_id": detail.id,
                            "position_in_order": detail.order_item.position_in_order,
                            "size": detail.size,
                            "type": detail.type,
                            "zapils_count": detail.zapils_count,
                        }
                        detail_and_profil_sublist.append(detail_dict)
                    print(f"detail_and_profil_sublist: {detail_and_profil_sublist}")
                    detail_and_profil_dict = {'profil_number' : profil_number, 'details' : detail_and_profil_sublist}
                    print(f"detail_and_profil_dict: {detail_and_profil_dict}")
                    details_list.append(detail_and_profil_dict)
                    profil_number += 1

                new_data.get(i).update({'raskroy': details_list})

    print(f"findDitailInDb data: {data}")
    print(f"findDitailInDb new_data: {new_data}")
    order = Order.objects.get(id = order_id)
    order.raskroy_json = new_data
    order.save()
    return new_data


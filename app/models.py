from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


def validate_only_one_instance(obj):
    model = obj.__class__
    if (model.objects.count() > 0 and
            obj.id != model.objects.get().id):
        raise ValidationError("Can only create 1 %s instance" % model.__name__)


class MyConstants(models.Model):
    zazor_sverhy_est_poperchnaya = models.IntegerField(default=1)
    zazor_sverhy_net_poperechnoi = models.IntegerField(default=5)
    zazor_sleva = models.IntegerField(default=3)
    zazor_sprava = models.IntegerField(default=3)
    zazor_snizy = models.IntegerField(default=15)
    likvidniy_ostatok = models.IntegerField(default=800)
    pilim_korobky_v_razmer_polotna_s_zapasom_mm = models.IntegerField(
        default=20)
    kontatknie_pyatna_mm = models.IntegerField(default=40)
    dlya_dverey_bez_verhnei_poperechini_plus_sverhy = models.IntegerField(
        default=50)
    tolshina_pila_mm = models.IntegerField(default=5)

    def __str__(self):
        return "Константы"

    class Meta:
        verbose_name_plural = 'Константы'

    def clean(self):
        validate_only_one_instance(self)


class Torec(models.Model):
    name = models.CharField(max_length=15)
    vneshn_razm_mm = models.FloatField(default=42)
    vnytr_razm_mm = models.FloatField(default=40)
    chetvert = models.BooleanField(default=False)
    dlina_mm = models.FloatField(default=6000)
    ves_metra_kg = models.FloatField(default=0.212)
    perimetr_mm = models.FloatField(default=150)
    kontaktnie_pyatna_mm = models.FloatField(default=40)
    vnytr_razm_dlya_petli_mm = models.FloatField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Торцы'


class Pokritie(models.Model):
    pokritie = models.CharField(max_length=25)

    def __str__(self):
        return self.pokritie

    class Meta:
        verbose_name_plural = 'Покрытия'


class Korobka(models.Model):
    name = models.CharField(max_length=15)
    razmer_pod_pramoe_polotno_bez_yteplitelya_mm = models.IntegerField(
        default=0)
    shirina_v_proeme_mm = models.FloatField(default=0)
    dlina_mm = models.IntegerField(default=5100)
    ves_metra_kg = models.FloatField(default=0.862)
    perimetr_mm = models.IntegerField(default=230)
    kontaktnie_pyatna_mm = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Коробки'


class KorobkaPolkiDlyaPetel(models.Model):
    """
     для одной коробки может быть несколько наборов петель, потому храним в
     отдельной таблице все пети и каждую из них связываем с какой-то
     коробкой.
    """

    korobka = models.ForeignKey(Korobka, on_delete=models.CASCADE)
    size_mm = models.FloatField(default=0)

    def __str__(self):
        return (str(self.korobka) + " " + str(self.size_mm))

    class Meta:
        verbose_name_plural = 'Коробки -> Полки для петель'

# комбинации коробка + торец, обеспечивающие прямое открывание


class KorobkaOpenByTorec(models.Model):
    korobka = models.ForeignKey(Korobka, on_delete=models.CASCADE)
    torec = models.ForeignKey(Torec, on_delete=models.CASCADE)

    def __str__(self):
        return (str(self.korobka) + "+" + str(self.torec))

    class Meta:
        verbose_name_plural = 'Прямое открытие (коробка + торец)'

# комбинации коробка + торец, обсепечивающие реверсивное открывание


class KorobkaReversByTorec(models.Model):
    korobka = models.ForeignKey(Korobka, on_delete=models.CASCADE)
    torec = models.ForeignKey(Torec, on_delete=models.CASCADE)

    def __str__(self):
        return (str(self.korobka) + "+" + str(self.torec))

    class Meta:
        verbose_name_plural = 'Реверсивное открытие (коробка + торец)'


class ProfilKarkasaPolotna(models.Model):
    name = models.CharField(max_length=15)
    vneshniy_razmier_mm = models.FloatField()
    vnytrenniy_razmer_mm = models.FloatField(max_length=vneshniy_razmier_mm)
    chetvert = models.BooleanField(default=False)
    pryamoe_otrk = models.BooleanField(default=True)
    revers = models.BooleanField(default=False)
    dlina_mm = models.FloatField(default=6000)
    ves_metra_kg = models.FloatField(default=0.983)
    perimetr_mm = models.FloatField(default=250)
    kontaktnie_pyatna_mm = models.FloatField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Профили каркаса полотна'


class SkritiePetli(models.Model):
    proizvoditel = models.CharField(max_length=20)
    model_name = models.CharField(max_length=20)
    opornaya_polka_mm = models.FloatField(default=0)
    shirina_dvernoi_chasti_mm = models.FloatField(default=0)
    shirina_korobochnoi_chasti_mm = models.FloatField(default=0)
    otstyp_ot_kraya_dveri_mm = models.FloatField(default=0)
    otstyp_ot_kraya_na_korobke_mm = models.FloatField(default=0)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return self.model_name

    class Meta:
        verbose_name_plural = 'Скрытые петли'


class Zamok(models.Model):
    name = models.CharField(max_length=20)
    visota_planki = models.FloatField(default=180)
    shirina_planki = models.FloatField(default=22)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Замки'

# уплотнитель


class Yplotnitel(models.Model):
    name = models.CharField(max_length=20)
    visota_mm = models.FloatField(default=10)
    tolshina_mm = models.FloatField(default=6)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return (str(self.name) + ": " + str(self.price) + "руб.")

    class Meta:
        verbose_name_plural = 'Уплотнитель'


class Ygolki(models.Model):
    name = models.CharField(max_length=25)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return (str(self.name) + ": " + str(self.price) + "руб.")

    class Meta:
        verbose_name_plural = 'Сухари/Уголки для сборки'


class MontajniePlastini(models.Model):
    name = models.CharField(max_length=25)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return (str(self.name) + ": " + str(self.price) + "руб.")

    class Meta:
        verbose_name_plural = 'Монтажные пластины'


class ShtukaturnayaSetka(models.Model):
    name = models.CharField(max_length=25)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return (str(self.name) + ": " + str(self.price) + "руб.")

    class Meta:
        verbose_name_plural = 'Штукатурная сетка'

# комбинации Торец+Коробка, которые работают для того или иного уплотнителя


class YplotnitelCombinations(models.Model):
    yplotnitel = models.ForeignKey(Yplotnitel, on_delete=models.CASCADE)
    korobka = models.ForeignKey(Korobka, on_delete=models.CASCADE)
    torec = models.ForeignKey(Torec, on_delete=models.CASCADE)

    def __str__(self):
        return (str(self.yplotnitel) + ": " +
                str(self.korobka) + "+" + str(self.torec))

    class Meta:
        verbose_name_plural = 'Уплотнитель -> торец + коробка'


# абстрактный класс для опций товаров
class ProductOptions(models.Model):
    product = None
    pokritie = models.ForeignKey(Pokritie, on_delete=models.RESTRICT)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    def __str__(self):
        return (str(self.product) + " " +
                str(self.pokritie) + " " + str(self.price))

    class Meta:
        abstract = True


class KorobkaOptions(ProductOptions):
    product = models.ForeignKey(Korobka, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Коробки -> Покрытие и цена'


class TorecOptions(ProductOptions):
    product = models.ForeignKey(Torec, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Торцы -> Покрытие и цена'


class ProfilKarkasaPolotnaOptions(ProductOptions):
    product = models.ForeignKey(ProfilKarkasaPolotna, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Профили каркаса полотна -> Покрытие и цена'


class Obrabotka(models.Model):
    name = models.CharField(max_length=60)
    const = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return (str(self.name) + " " + str(self.const))

    class Meta:
        verbose_name_plural = 'Обработка'


class ObrabotkaTest(models.Model):
    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        blank=True,
        null=True)
    name = models.CharField(max_length=60)
    const = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return (str(self.name) + " " + str(self.const))

    class Meta:
        verbose_name_plural = 'Обработка тест'


class TestModelForApi(models.Model):
    name1 = models.CharField(max_length=60)
    name2 = models.CharField(max_length=60)

    @property
    def name3(self):
        return self.name1 + self.name2

    def __str__(self):
        return (self.name1)

    class Meta:
        verbose_name_plural = 'Тестовая модель для API'


class OrderItem(models.Model):
    order_id = models.ForeignKey('Order', on_delete=models.CASCADE)
    position_in_order = models.IntegerField()
    sticker = models.CharField(max_length=40, blank=True, null=True)
    polotno_height = models.IntegerField()
    polotno_width = models.IntegerField()
    storona_otkr = models.IntegerField(
        default=0)  # 1, 2 не реверс, 3, 4 - реверс
    color = models.ForeignKey(Pokritie, on_delete=models.RESTRICT)
    zamok = models.ForeignKey(
        Zamok,
        on_delete=models.RESTRICT,
        blank=True,
        null=True)
    petli = models.ForeignKey(
        SkritiePetli,
        on_delete=models.RESTRICT,
        blank=True,
        null=True)
    petli_count = models.IntegerField(blank=True, null=True)
    korobka = models.ForeignKey(
        Korobka,
        on_delete=models.RESTRICT,
        blank=True,
        null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    vertikal_length1 = models.IntegerField(blank=True, null=True)
    vertikal_length2 = models.IntegerField(blank=True, null=True)
    horizontal_length = models.IntegerField(blank=True, null=True)
    top_poperechina = models.BooleanField(blank=True, null=True)

    @property
    def shirina_v_proeme(self):
        return self.korobka.shirina_v_proeme_mm

    @property
    def zagotovka_dlina(self):
        return self.korobka.dlina_mm

    def __str__(self):
        return ("#" +
                str(self.id) +
                " | " +
                str(self.order_id) +
                " | " +
                str(self.sticker))

    class Meta:
        verbose_name_plural = 'Подзаказы заказов'


class Order(models.Model):

    manager = models.ForeignKey(
        User,
        on_delete=models.RESTRICT,
        related_name='manager')
    otvetstvenniy_za_proizvodstvo = models.ForeignKey(User, on_delete=models.RESTRICT, related_name='otvetstvenniy_za_proizvodstvo')
    description = models.CharField(blank=True, null=True, max_length=150)
    client = models.CharField(max_length=50)

    # комплектация
    dvernaya_korobka = models.BooleanField(default=False)
    torec_karkas_polotna = models.BooleanField(default=False)
    petli = models.BooleanField(default=False)
    zamok_and_otvestka = models.BooleanField(default=False)
    yplotnitel = models.BooleanField(default=False)
    syhori_ygolki = models.BooleanField(default=False)
    montajnie_plastini = models.BooleanField(default=False)
    shtykatyrnaya_setka = models.BooleanField(default=False)

    # обработка
    bez_obrabotki = models.BooleanField(default=False)
    pilim_dlya_transportnoi = models.BooleanField(default=False)
    mehobrabotka = models.BooleanField(default=False)
    pilim_korobky_v_razmer_polotna = models.BooleanField(default=False)
    pilim_torec_v_razmer_polotna_s_zapasom = models.BooleanField(default=False)
    pilim_torec_v_razmer_polotna = models.BooleanField(default=False)
    zapas_mm = models.IntegerField(default=0)
    pilim_torec_v_razmer_polotna_s_zapilom = models.BooleanField(default=False)
    otrezaem_kontatknie_pyatna = models.BooleanField(default=False)
    kontaktnie_pyatna_mm = models.IntegerField(default=0)
    frezerovka_korobki_por_petli_i_ovesky_zamka = models.BooleanField(default=False)
    frezerovka_torca_or_karkasa_pod_petli_i_zamok = models.BooleanField(default=False)
    frezerovka_pod_nestandartnyy_fyrnityry = models.BooleanField(default=False)
    na_niz_revers_poloten_pryamoi_torec = models.BooleanField(default=False)
    na_verh_poloten_bez_poperechini_pryamoi_torec = models.BooleanField(default=False)
    dlya_dverey_bez_verhn_poperechini_plus = models.BooleanField(default=False)
    dlya_dverey_bez_verhnei_poperechini_plus_mm = models.IntegerField(blank=True, null=True)

    raskroy_json = models.JSONField(default=None, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return ("#" +
                str(self.id) +
                " | " +
                str(self.manager) +
                " | " +
                str(self.description))

    def __int__(self):
        return (int(self.id))

    class Meta:
        verbose_name_plural = 'Заказы'


class OrderDetails(models.Model):
    """
    модель для хранения запилов деталей(кусков профиля) заказа
    необходима для получения информации о точках реза
    used - флаг, устанавливается в True, если мы уже нашли и взяли эту
    деталь из алгоритма раскроя. Алгоритм раскроя не может принимать id-шники и запилы из за падения
    скорости рассчета, а у деталей может быть одинаковая длина в одном
    заказе(что мешает их идендитифицировать), поэтому
    пишем более сложную реализацию через бд и флаг.
    type - тип детали (false - часть коробки, true - один из торцов)
    zapils_count - количество запилов под 45 градусов
    """
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='order_id')
    order_item = models.ForeignKey(
        OrderItem,
        on_delete=models.CASCADE,
        related_name='order_item_id')
    size = models.IntegerField()
    zapils_count = models.IntegerField(default=0)
    type = models.BooleanField(default=False)
    profil_torca = models.ForeignKey(
        Torec,
        on_delete=models.RESTRICT,
        related_name='torec_id',
        blank=True,
        null=True)
    used = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Детали заказа'

    def __str__(self):
        return (str(self.id) + " | " + str(self.order.id) + " | " +
                str(self.order_item.id) + " | " + str(self.type))


class MehObrabotkaPrice(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField(default=0)
    price2 = models.FloatField(default=0)
    price3 = models.FloatField(default=0)
    price4 = models.FloatField(default=0)

    class Meta:
        verbose_name_plural = 'Прайс на мехобработку'

    def __str__(self):
        return (str(self.name) + " | " + str(self.price))

from django.shortcuts import render
from . import models
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import action
from . import serializers
from django.http import JsonResponse
from django.db.models import Q
import time
from . import services
from . import helpers
# алгоритм линейного раскроя
from .busines_services.lineyniy_raskroy import optimizacia6


def index(request):
    torci = models.Torec.objects.all()
    korobki = models.Korobka.objects.all()
    petli = models.SkritiePetli.objects.order_by('-proizvoditel').all()
    zamki = models.Zamok.objects.all()
    pokritie = models.Pokritie.objects.all()
    constants = models.MyConstants.objects.get(pk=1)
    rabotyagi_list = models.User.objects.filter(groups__name='proizvodstvo')
    manager = request.user
    context = {
        'torci': torci,
        'korobki': korobki,
        'petli': petli,
        'zamki': zamki,
        'pokritie': pokritie,
        'constants': constants,
        'rabotyagi_list': rabotyagi_list,
        'manager': manager
    }

    return render(request, 'app/index.html', context)


def updateOrderItems(request):
    """
    Обновление информации о позициях заказа,
    Генерация и получение информации о линейном раскрое, пилах, и прочих деталях заказа
    :param type_detail, если true, то ищим коробки, если false, то торцы
    """
    time.sleep(5)
    order_id = request.POST.get('order_id')
    type_detail = request.POST.get('type')
    if type_detail == 'true':
        type_detail = True
    if type_detail == 'false':
        type_detail = False
    print(f"updateOrderItems type: {type_detail}, {type(type_detail)}")
    print("updateOrderItems order_id: ", order_id)
    korobki_in_order = models.OrderItem.objects.filter(
        order_id=order_id).values('korobka').distinct()
    print(f"korobki_in_order: {korobki_in_order}")
    result_full = services.getResult(korobki_in_order, order_id, type_detail)
    print(f"updateOrderItems->result_full: {result_full}")
    #time.sleep(5)
    find_details_in_db = services.findDitailInDb(order_id, result_full, type=type_detail)
    print(f"updateOrderItems->find_details_in_db: {find_details_in_db}")
    return JsonResponse(find_details_in_db)


class TestApiViewSet(viewsets.ModelViewSet):
    queryset = models.TestModelForApi.objects.all()
    serializer_class = serializers.TestSerializer
    permission_classes = [permissions.AllowAny]


class OrderViewSet(viewsets.ModelViewSet):
    queryset = models.Order.objects.all()
    serializer_class = serializers.OrderSerializer
    permission_classes = [permissions.AllowAny]

    @action(detail=True, methods=['post'])
    def createOrder(self, request):
        order = models.Order
        order.manager = request.user


class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = models.OrderItem.objects.all()
    serializer_class = serializers.OrderItemSerializer
    permission_classes = [permissions.AllowAny]

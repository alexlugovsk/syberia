from .models import TestModelForApi, Order, OrderItem
from rest_framework import serializers
from pprint import pprint


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModelForApi
        fields = ['name1', 'name2', 'id']

    def create(self, data):
        pprint(data)
        return TestModelForApi.objects.create(
            name1=data.get('name1'),
            name2=data.get('name2')
        )


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'

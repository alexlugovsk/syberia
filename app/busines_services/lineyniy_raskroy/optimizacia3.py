import time

class optimization_cutting:
    def __init__(self, all_list, size_blank):
        self.all_dict = {}
        self.size_blank = size_blank
        self.combination_list = {}
        self.min_size_raskroi = len(all_list)
        self.max_ostatok = 0
        self.luchiy_raskroy = {}

        for item in all_list:
            if self.all_dict.get(item[-1]) is None:
                self.all_dict[item[-1]] = [item]
            else:
                self.all_dict[item[-1]].append(item)

    def __combination(self, list_1, list_2, size_i):
        for i, item in enumerate(list_2):
            if self.size_blank - sum([j[0] for j in list_1]) >= item[0]:
                l1, l2 = list_1.copy(), list_2.copy()
                l1.append(l2.pop(i))
                self.__combination(l1, l2[i:], size_i)
            else:
                if self.combination_list.get(size_i) is None:
                    self.combination_list[size_i] = []
                else:
                    self.combination_list[size_i].append(list_1)
                break

    def __all_raskroy(self, list_1, combination_list, size_i):
        dil_list = set([item for sublist in list_1 for item in sublist])
        [combination_list.remove(i) for i in combination_list if len(dil_list & set(i)) > 0]

        if len(combination_list) > 0:
            [self.__all_raskroy(list_1 + [next_item], combination_list[i:], size_i) for i, next_item in enumerate(combination_list)]
        elif sum(set([i[0] for i in self.all_dict[size_i]]).difference(k[0] for k in dil_list)) <= self.size_blank:
            if len(list(set(self.all_dict[size_i]).difference(dil_list))) != 0:
                list_1 += [list(set(self.all_dict[size_i]).difference(dil_list))]
            if len(list_1) == self.min_size_raskroi and max([self.size_blank - sum([j[0] for j in i]) for i in list_1]) > self.max_ostatok:
                self.luchiy_raskroy[size_i] = list_1
                self.max_ostatok = max([self.size_blank - sum([j[0] for j in i]) for i in list_1])
            elif len(list_1) < self.min_size_raskroi:
                self.min_size_raskroi = len(list_1)
                self.max_ostatok = max([self.size_blank - sum([j[0] for j in i]) for i in list_1])
                self.luchiy_raskroy[size_i] = list_1

    def get_raskroy(self):
        for size_i, item in self.all_dict.items():
            self.all_dict[size_i] = sorted(self.all_dict[size_i])
            self.all_dict[size_i].append((self.size_blank, True, False, 1))
            [self.__combination([self.all_dict[size_i][Iterator]], self.all_dict[size_i][Iterator + 1:], size_i) for Iterator in range(len(self.all_dict[size_i]) - 1)]
            self.all_dict[size_i].pop(-1)

        for size_i, item in self.combination_list.items():
            self.luchiy_raskroy[size_i] = []
            [self.__all_raskroy([start_item], self.combination_list[size_i][i:], size_i) for i, start_item in
             enumerate(item)]
        return self.luchiy_raskroy


if __name__ == '__main__':
    proba_1 = optimization_cutting([(23, True, False, 31),
                                    (86, True, False, 31),
                                    (91, True, False, 31),
                                    (61, True, False, 31),
                                    (135, True, False, 31),
                                    (78, True, False, 31),
                                    (122, True, False, 31),
                                    (97, True, False, 35),
                                    (95, True, False, 35),
                                    (81, True, False, 35),
                                    (58, True, False, 35)], 200)

    proba_2 = optimization_cutting([(23, True, False, 31),
                                    (86, True, False, 31),
                                    (91, True, False, 31),
                                    (61, True, False, 31),
                                    (135, True, False, 31),
                                    (78, True, False, 31),
                                    (122, True, False, 31),
                                    (86, True, False, 31),
                                    (91, True, False, 31),
                                    (61, True, False, 31),
                                    (135, True, False, 31),
                                    (78, True, False, 31),
                                    (122, True, False, 31),
                                    (86, True, False, 31),
                                    (91, True, False, 31),
                                    (61, True, False, 31),
                                    (135, True, False, 31),
                                    (78, True, False, 31),
                                    (122, True, False, 31),
                                    (58, True, False, 31)], 200)

    proba_3 = optimization_cutting([(23, False, True, 31, 'id'),
                                    (86, False, True, 31, 'id'),
                                    (86, False, True, 31, 'id'),
                                    (91, True, False, 31, 'id'),
                                    (61, True, False, 31, 'id'),
                                    (135, True, True, 31, 'id'),
                                    (86, True, True, 31, 'id'),
                                    (86, True, True, 31, 'id'),
                                    (91, True, True, 31, 'id'),
                                    (61, True, True, 31, 'id'),
                                    (135, False, False, 31, 'id'),
                                    (85, False, False, 31, 'id'),
                                    (81, False, False, 31, 'id'),
                                    (72, False, False, 31, 'id'),
                                    (78, True, False, 31, 'id'),
                                    (180, True, False, 31, 'id'),
                                    (200, True, False, 31, 'id')], 200)

    proba_3 = optimization_cutting([(2000, False, True, 31),
                                    (2000, False, True, 31),
                                    (800, False, True, 31),
                                    (2100, True, False, 31),
                                    (2100, True, False, 31),
                                    (810, True, True, 31),
                                    (1900, True, True, 31),
                                    (1900, False, False, 31),
                                    (800, True, True, 31),
                                    (1950, True, True, 31),
                                    (850, False, False, 31),
                                    (1950, True, False, 31)], 6000)
    start_time = time.time()
    print(proba_3.get_raskroy())
    print("--- %s seconds ---" % (time.time() - start_time))

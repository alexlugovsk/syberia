import time
start_time = time.time()

class optimization_cutting:
    def __init__(self, all_list, size_blank):
        self.all_list = sorted(all_list)
        self.size_blank = size_blank
        self.combination_list = []
        self.all_raskroy_list = []
        self.raskroy_size_blank = {}
        self.max_ostatok = 0
        self.luchiy_raskroy = []

    def __combination(self, list_1, list_2):
        x = True
        for i, item in enumerate(list_2):
            if self.size_blank - sum(list_1) >= item:
                l1, l2 = list(tuple(list_1)), list(tuple(list_2))
                l1.append(l2.pop(i))
                self.__combination(l1, l2[i:])
                x = False
            elif x:
                self.combination_list.append(list_1)
                break

    def __all_raskroy(self, list_1, combination_list):
        combination_list = list(combination_list)
        dil_list = set([item for sublist in list_1 for item in sublist])
        for i_start_item in dil_list:
            for pop_i in [st for st in combination_list if i_start_item in st]:
                combination_list.remove(pop_i)
        if len(combination_list) > 0:
            for i, next_item in enumerate(combination_list):
                self.__all_raskroy(list_1 + [next_item], tuple(combination_list[i:]))
        elif sum(set(self.all_list).difference(dil_list)) <= self.size_blank:
            if len(list(set(self.all_list).difference(dil_list))) != 0:
                list_1 += [list(set(self.all_list).difference(dil_list))]
            self.all_raskroy_list.append(list_1)

    def get_raskroy(self):
        self.all_list.append(self.size_blank)
        for Iterator in range(len(self.all_list) - 1):
            self.__combination([self.all_list[Iterator]], self.all_list[Iterator + 1:])
        self.all_list.pop(-1)

        for start_item in self.combination_list:
            self.__all_raskroy([start_item], tuple(self.combination_list))

        for i in self.all_raskroy_list:
            if self.raskroy_size_blank.get(len(i)) is None:
                self.raskroy_size_blank[len(i)] = [i]
            else:
                self.raskroy_size_blank[len(i)].append(i)

        for item_raskroy in self.raskroy_size_blank[sorted(self.raskroy_size_blank)[0]]:
            for sum_blank in item_raskroy:
                if self.size_blank - sum(sum_blank) > self.max_ostatok:
                    self.luchiy_raskroy = item_raskroy
        return self.luchiy_raskroy


if __name__ == '__main__':
    proba_1 = optimization_cutting([23, 78, 58, 61, 81, 86, 91, 95, 97], 200)

    start_time = time.time()
    proba_2 = optimization_cutting([78, 78, 50, 61, 61, 48, 91, 95, 97, 23, 78, 56, 78, 81], 200)
    print(proba_2.get_raskroy())
    print("--- %s seconds ---" % (time.time() - start_time))
    print(proba_2.combination_list)
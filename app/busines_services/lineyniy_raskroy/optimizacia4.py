#import time #for tests

class optimization_cutting:
    def __init__(self, all_list, size_blank):
        self.all_list = sorted(all_list)
        self.size_blank = size_blank
        self.combination_list = []

        self.raskroy_size_blank = len(all_list)
        self.min_raskroy = self.size_blank

        self.luchiy_raskroy = []

    def __combination(self, list_1, list_2):
        x = True
        for i, item in enumerate(list_2):
            if self.size_blank - sum(list_1) >= item:
                x = False
                l1, l2 = list_1.copy(), list_2.copy()
                l1.append(l2.pop(i))
                self.__combination(l1, l2[i:])
            elif x:
                if list_1 not in self.combination_list:
                    self.combination_list.append(list_1)
                break

    def __all_raskroy(self, list_1, combination_list):
        dil_list = [item for sublist in list_1 for item in sublist]
        [[combination_list.remove(st) for st in combination_list.copy() if i_start_item in st and dil_list.count(i_start_item) + st.count(i_start_item) > self.all_list.count(i_start_item)] for i_start_item in dil_list]

        if len(combination_list) > 0:
            for i, next_item in enumerate(combination_list):
                self.__all_raskroy(list_1 + [next_item], combination_list[i:])
        elif sum(self.all_list) - sum([item for sublist in list_1 for item in sublist]) <= self.size_blank:
            add_list = self.all_list.copy()
            [add_list.remove(i) for i in [item for sublist in list_1 for item in sublist]]
            list_1.append(add_list)
            if len(list_1) < self.raskroy_size_blank:
                self.raskroy_size_blank = len(list_1)
                self.min_raskroy = min([sum(i) for i in list_1])
                self.luchiy_raskroy = list_1
            elif len(list_1) == self.raskroy_size_blank:
                if min([sum(i) for i in list_1]) < self.min_raskroy:
                    self.min_raskroy = min([sum(i) for i in list_1])
                    self.luchiy_raskroy = list_1

    def get_raskroy(self):
        self.all_list.append(self.size_blank)
        [self.__combination([self.all_list[Iterator]], self.all_list[Iterator + 1:]) for Iterator in
         range(len(self.all_list) - 1)]
        self.all_list.pop(-1)

        self.combination_list.reverse()

        max_sum = 0
        for i in reversed(self.all_list.copy()):
            if max_sum + i <= self.size_blank:
                max_sum += i
            else:
                for j, start_item in enumerate(self.combination_list):
                    if start_item[0] >= i:
                        print(start_item)
                        self.__all_raskroy([start_item], self.combination_list.copy()[j:])
                break

        return self.luchiy_raskroy

"""
if __name__ == '__main__':
    start_time = time.time()
    proba_1 = optimization_cutting((23, 23, 23, 58, 61, 78, 81, 86, 91, 95, 97, 86, 91, 95), 200)
    print(proba_1.get_raskroy())
    print("--- %s seconds ---" % (time.time() - start_time))

    start_time2 = time.time()
    proba_2 = optimization_cutting((23, 23, 23, 58, 61, 78, 81, 86, 91, 95, 97, 86, 91), 200)
    print(proba_2.get_raskroy())
    print("--- %s seconds ---" % (time.time() - start_time2))
"""
class optimization_cutting:
    def __init__(self, all_list, size_blank):
        self.all_list = all_list
        self.size_blank = size_blank
        self.combination_list = []
        self.raskroy = []

    def __combination(self, list_1, list_2):
        for item in list_2:
            if self.size_blank - sum(list_1) >= item:
                l1 = sorted(list(set(list_1)))
                l2 = sorted(list(set(list_2)))
                if l1[-1] < item:
                    l1.append(item)
                l2.remove(item)
                self.__combination(l1, l2)
            elif 0 <= self.size_blank - sum(list_1) < sorted(list(set(self.all_list).difference(list_1)))[0]:
                self.combination_list.append(list_1)
                break

    def start_combination(self):
        for Iterator in range(len(self.all_list) - 1):
            self.__combination([self.all_list[Iterator]], self.all_list[Iterator + 1:])
        n = []
        for i in self.combination_list:
            if i not in n:
                n.append(i)
        self.combination_list = n
        return self.combination_list

    def __all_raskroy(self, list_1, combination_list):
        combination_list = list(combination_list)
        for i_start_item in list_1:
            for pop_i in [st for st in combination_list if i_start_item in st]:
                combination_list.remove(pop_i)
        if len(combination_list) > 0:
            for next_item in combination_list:
                self.__all_raskroy(list_1 + next_item, combination_list)
        else:
            self.raskroy.append(list(set(list_1).union(set(self.all_list))))

    def give_raskroy(self):
        self.start_combination()
        for j in reversed(self.all_list):
            for start_item in [st for st in self.combination_list if j in st]:
                self.__all_raskroy(start_item, tuple(self.combination_list))
        n = []
        max_size_list = len(max(self.raskroy))
        for i in self.raskroy:
            if i not in n and len(i) == max_size_list:
                n.append(i)

        # ниже разбиваем заготовку на детали
        all_raskroy_tuple = []
        for item in n:
            srez_list = []
            srez_list_last = []
            for namber in item:
                if sum(srez_list_last) + namber <= self.size_blank:
                    srez_list_last.append(namber)
                else:
                    srez_list.append(tuple(srez_list_last))
                    srez_list_last.clear()
                    srez_list_last.append(namber)
            if len(srez_list_last) != 0:
                srez_list.append(tuple(srez_list_last))
            all_raskroy_tuple.append(srez_list)

        ostatok = []
        for i in all_raskroy_tuple:
            ostatok_item = len(i) * self.size_blank
            for j in i:
                ostatok_item -= sum(j)
            ostatok.append(ostatok_item)
        self.raskroy = dict(zip(ostatok, all_raskroy_tuple))
        return self.raskroy


if __name__ == '__main__':
    proba_1 = optimization_cutting(sorted([15, 25, 39, 45, 56, 67, 78, 88, 123, 198]), 300)
    proba_2 = optimization_cutting(sorted([100, 100, 48, 95, 95, 78, 120, 120, 80, 120, 120, 80, 120, 120, 99, 100, 100, 40, 106,
                                           106, 66, 100, 100, 101]), 300)
    print(proba_1.give_raskroy())
    print(proba_2.give_raskroy())
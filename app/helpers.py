def splitTuple(obj_tuple, n):
    """
    Разбиваем кортеж на указанные части
    ojb_tuple - кортеж
    n - по сколько элементов нужно в каждом кортеже
    """
    result = tuple(obj_tuple[i:i + n] for i in range(0, len(obj_tuple), n))
    return result


